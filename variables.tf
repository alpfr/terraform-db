variable "rds_subnet1" {
  description = "Provide the subnet 1"
  type        = string
  default     = "subnet-00b8f4edfb9bdd684"
}

variable "region" {
  description = "Provides details about a specific AWS region."
  type        = string
  default     = "us-west-2"
}

variable "rds_subnet2" {
  description = "Provide the subnet 2"
  type        = string
  default     = "subnet-0281e47e182058947"
}

variable "db_instance" {
  description = "Provides the db instance type."
  type        = string
  default     = "db.t3.small"
}

variable "vpc_id" {
  description = "Provides the VPC ID."
  type        = string
  default     = "vpc-0e8b31c3d2d87e09f"
}
